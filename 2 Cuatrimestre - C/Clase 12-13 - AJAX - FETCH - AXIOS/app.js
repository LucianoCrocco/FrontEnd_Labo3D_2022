import {
    getPersonaAjax,
    getPersonasAjax,
    getPersonasAjaxPromise,
    getPersonasAjaxAsync,
    postPersonaAjax,
    putPersonaAjax,
    deletePersonaAjax,
} from "./js/ajax.js";
import {
    getPersonaFetch,
    getPersonasFetch,
    getPersonasFetchAsync,
    postPersonaFetch,
} from "./js/fetch.js";
import {
    getPersonaAxios,
    getPersonasAxios,
    getPersonasAxiosAsync,
    postPersonaAxios,
} from "./js/axios.js";
const URL = "http://localhost:3001/personas";

export const cargarSpinner = () => {
    const divSpinner = document.querySelector(".spinner");
    if (!divSpinner.hasChildNodes()) {
        const spinner = document.createElement("img");
        spinner.setAttribute("src", "./Rocket.gif");
        spinner.setAttribute("alt", "Icono spinner");
        divSpinner.appendChild(spinner);
    }
};

export const eliminarSpinner = () => {
    const divSpinner = document.querySelector(".spinner");
    while (divSpinner.hasChildNodes()) {
        divSpinner.removeChild(divSpinner.firstChild);
    }
};

/* GET - EVENTOS */
// GET persona AJAX
document.getElementById("btn1").addEventListener("click", () => {
    getPersonasAjax(URL);
});
// GET persona Fetch
document.getElementById("btn2").addEventListener("click", () => {
    getPersonasFetch(URL);
});
// GET persona Axios
document.getElementById("btn3").addEventListener("click", () => {
    getPersonasAxios(URL);
});
// GET personas AJAX
document.getElementById("btn4").addEventListener("click", () => {
    getPersonaAjax(URL, 5);
});
// GET personas Fetch
document.getElementById("btn5").addEventListener("click", () => {
    getPersonaFetch(URL, 4);
});
// GET personas Axios
document.getElementById("btn6").addEventListener("click", () => {
    getPersonaAxios(URL, 3);
});
// GET personas Fetch Async
document.getElementById("btn7").addEventListener("click", () => {
    getPersonasFetchAsync(URL);
});
// GET personas Axios Async
document.getElementById("btn8").addEventListener("click", () => {
    getPersonasAxiosAsync(URL);
});
//GET personas Ajax Promise
document.getElementById("btn9").addEventListener("click", () => {
    cargarSpinner();
    getPersonasAjaxPromise(URL)
        .then((res) => console.log(res))
        .catch((err) => console.log(err))
        .finally(() => eliminarSpinner());
});
//GET personas Ajax Async
document.getElementById("btn10").addEventListener("click", () => {
    getPersonasAjaxAsync(URL);
});

/* POST - EVENTOS */
//POST persona Ajax
document.getElementById("btn11").addEventListener("click", () => {
    postPersonaAjax(URL);
});
//POST persona Fetch
document.getElementById("btn12").addEventListener("click", () => {
    postPersonaFetch(URL);
});
//POST persona Axios
document.getElementById("btn13").addEventListener("click", () => {
    postPersonaAxios(URL);
});

/* DELETE - EVENTOS */
//DELETE persona Ajax
document.getElementById("btn14").addEventListener("click", () => {
    deletePersonaAjax(URL, 1);
});

/* PUT -EVENTOS */
//PUT persona Ajax
document.getElementById("btn15").addEventListener("click", () => {
    putPersonaAjax(URL);
});
