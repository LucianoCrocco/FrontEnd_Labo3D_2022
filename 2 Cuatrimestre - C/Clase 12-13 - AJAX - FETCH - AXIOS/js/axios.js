import { cargarSpinner, eliminarSpinner } from "../app.js";
const newPersona = {
    nombre: "RobertoAXIOS",
    edad: 54,
    genero: "M",
};

//GET personas PROMISE
const getPersonasAxios = (url) => {
    cargarSpinner();

    axios
        .get(url)
        .then((res) => console.log(res.data))
        .catch((err) => {
            console.error(err.message);
        })
        .finally(() => eliminarSpinner());
};

//GET persona PROMISE
const getPersonaAxios = (url, id) => {
    cargarSpinner();

    axios
        .get(`${url}/${id}`)
        .then((res) => console.log(res.data))
        .catch((err) => {
            console.error(err.message);
        })
        .finally(() => eliminarSpinner());
};

//GET persona ASYNC-AWAIT
const getPersonasAxiosAsync = async (url) => {
    try {
        cargarSpinner();
        const res = await axios.get(url);
        console.log(res);
    } catch (err) {
        console.error(err.message);
    } finally {
        eliminarSpinner();
    }
};

//POST persona
const postPersonaAxios = (url) => {
    cargarSpinner();
    const options = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        data: JSON.stringify(newPersona),
    };
    axios
        .post(url, options)
        .then((res) => console.log(res.data))
        .catch((err) => {
            console.error(err.message);
        })
        .finally(() => eliminarSpinner());
};

export {
    getPersonaAxios,
    getPersonasAxios,
    getPersonasAxiosAsync,
    postPersonaAxios,
};
