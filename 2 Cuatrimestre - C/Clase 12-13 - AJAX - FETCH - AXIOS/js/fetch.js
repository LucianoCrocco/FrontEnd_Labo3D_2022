import { cargarSpinner, eliminarSpinner } from "../app.js";
const newPersona = {
    nombre: "PedroFETCH",
    edad: 54,
    genero: "M",
};

//GET personas PROMISE
const getPersonasFetch = (url) => {
    cargarSpinner();
    fetch(url)
        .then((res) => {
            //console.log(res)
            return res.ok ? res.json() : Promise.reject(res);
        })
        .then((data) => console.log(data))
        .catch((err) => {
            console.error(err.status, err.statusText);
        })
        .finally(() => eliminarSpinner());
};

//GET persona PROMISE
const getPersonaFetch = (url, id) => {
    cargarSpinner();
    fetch(`${url}/${id}`)
        .then((res) => {
            //console.log(res)
            return res.ok ? res.json() : Promise.reject(res);
        })
        .then((data) => console.log(data))
        .catch((err) => {
            console.error(err.status, err.statusText);
        })
        .finally(() => eliminarSpinner());
};

//GET persona ASYNC-AWAIT
const getPersonasFetchAsync = async (url) => {
    try {
        cargarSpinner();
        const res = await fetch(url);
        if (!res.ok) {
            throw Promise.reject(res);
        }
        const data = await res.json();
        console.log(data);
    } catch (err) {
        err.catch(() => console.log(err));
        console.error(err.status, err.statusText);
    } finally {
        eliminarSpinner();
    }
};

//POST persona
const postPersonaFetch = (url) => {
    cargarSpinner();
    const options = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(newPersona),
    };
    fetch(url, options)
        .then((res) => {
            return res.ok ? res.json() : Promise.reject(res);
        })
        .then((data) => console.log(data))
        .catch((err) => {
            console.error(err.status, err.statusText);
        })
        .finally(() => eliminarSpinner());
};

export {
    getPersonaFetch,
    getPersonasFetch,
    getPersonasFetchAsync,
    postPersonaFetch,
};
