import { cargarSpinner, eliminarSpinner } from "../app.js";
const newPersona = {
    nombre: "JuanAJAX",
    edad: 54,
    genero: "M",
};

//GET personas
const getPersonasAjax = (url) => {
    const xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", () => {
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status < 300) {
                const data = JSON.parse(xhr.responseText);
                console.log(data);
            } else {
                console.error(xhr.status, xhr.statusText);
            }
            eliminarSpinner();
        } else {
            cargarSpinner();
        }
    });
    xhr.open("GET", url);
    xhr.send();
};

//GET persona
const getPersonaAjax = (url, id) => {
    const xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", () => {
        //console.log("Hola");
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status < 300) {
                const data = JSON.parse(xhr.responseText);
                console.log(data);
            } else {
                console.error(xhr.status, xhr.statusText);
            }
            eliminarSpinner();
        } else {
            cargarSpinner();
        }
    });
    //La configuracion del servidor.
    //xhr.open("GET", `${url}/${id}`);
    xhr.open("GET", `${url}?id=${id}`);
    xhr.send();
};

//GET persona PROMISE -> ASYNC-AWAIT: Creamos una funcion que devuelva un promesa para luego poder hacer una funcion que pueda utilizar AJAX con async-await
const getPersonasAjaxPromise = (url) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.addEventListener("readystatechange", () => {
            if (xhr.readyState == 4) {
                if (xhr.status >= 200 && xhr.status < 300) {
                    const data = JSON.parse(xhr.responseText);
                    resolve(data);
                } else {
                    reject({ status: xhr.status, statusText: xhr.statusText });
                }
            }
        });
        xhr.open("GET", url);
        xhr.send();
    });
};
const getPersonasAjaxAsync = async (url) => {
    try {
        cargarSpinner();
        const data = await getPersonasAjaxPromise(url);
        console.log(data);
    } catch (err) {
        console.log(err);
    } finally {
        eliminarSpinner();
    }
};

//POST persona
const postPersonaAjax = (url) => {
    const xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", () => {
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status < 300) {
                const data = JSON.parse(xhr.responseText);
                alert(`${data.id} ${data.nombre} ${data.edad} ${data.genero}`);
            } else {
                console.error(xhr.status, xhr.statusText);
            }
            eliminarSpinner();
        } else {
            cargarSpinner();
        }
    });
    xhr.open("POST", url);
    xhr.setRequestHeader("Content-Type", "application/json;charset=utf8");
    xhr.send(JSON.stringify(newPersona)); // Se lo mando como texto
};

//DELETE persona
const deletePersonaAjax = (url, id) => {
    const xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", () => {
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status < 300) {
                const data = JSON.parse(xhr.responseText);
                console.log(data);
            } else {
                console.error(xhr.status, xhr.statusText);
            }
            eliminarSpinner();
        } else {
            cargarSpinner();
        }
    });
    xhr.open("DELETE", `${url}/${id}`);
    xhr.send();
};

//PUT persona
const putPersonaAjax = (url) => {
    const updatedPersona = {
        id: 2,
        nombre: "SergioAJAX",
        edad: 22,
        genero: "M",
    };
    const xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", () => {
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status < 300) {
                const data = JSON.parse(xhr.responseText);
                console.log(data);
            } else {
                console.error(xhr.status, xhr.statusText);
            }
            eliminarSpinner();
        } else {
            cargarSpinner();
        }
    });
    xhr.open("PUT", `${url}/${updatedPersona.id}`);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(updatedPersona));
};

export {
    getPersonaAjax,
    getPersonasAjax,
    getPersonasAjaxPromise,
    getPersonasAjaxAsync,
    postPersonaAjax,
    putPersonaAjax,
    deletePersonaAjax,
};
