import {
    validarCampoVacio,
    validarPrecio,
    validarTexto,
    validarTodosLosCampos,
    clearError,
} from "./validaciones.js";
import { funcionesScript } from "./scripts.js";
import Anuncio_Animal from "./Anuncio_Animal.js";
import crearTabla from "./tabla.js";
import {
    deleteMascotaAjaxPromise,
    getMascotaAjax,
    getMascotasAjaxAsync,
    postMascotaAjaxAsync,
    putMascotaAjaxPromise,
} from "./ajax.js";

const controles = document.forms[0].elements;
const frm = document.getElementById("form-principal");

for (let i = 0; i < controles.length; i++) {
    const control = controles.item(i);
    if (control.matches("input")) {
        if (control.matches("[type=text]")) {
            control.addEventListener("blur", validarCampoVacio);
            control.addEventListener("input", validarTexto);
        } else if (control.matches("[type=number]")) {
            control.addEventListener("blur", validarCampoVacio);
            control.addEventListener("input", validarPrecio);
        } else if (control.matches("[type=date]")) {
            control.addEventListener("input", validarTexto);
            control.addEventListener("blur", validarCampoVacio);
        }
    }
}

// FORM ABM
/* Elementos del DOM */
const btnPrincipal = document.getElementById("btnPrincipal");
const containerTabla = document.getElementById("tabla-container");
const containerBotones = document.getElementById("botones-container");
const containerFiltros = document.getElementById("filtros-container");

/* ID general para modificar o eliminar */
let id = null;

/* Lista del JSON-SERVER y generacion de la tabla */
const url = "http://localhost:3001/mascotas";
//! PROFESOR: USE TOP LEVEL AWAIT YA QUE EN TEORIA ESTA BIEN SEGUN LA DOCUMENTACION. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await#top_level_await - TAMBIEN ESTA SOPORTADO EN LA MAYORIA DE NAVEGADORES: https://caniuse.com/?search=top%20level%20await
//! PROFESOR: Live Server se recarga al usar PUT, POST, DELETE. Solo con GET no genera una recarga. Utilizando la extension vs-code-preview-server no se refresca. Puede ser algo propio mio y que a usted no le suceda. Solo le aviso https://github.com/ritwickdey/vscode-live-server/issues/150
//* Sin el top level await podria haber hecho una closure o algo similar para cargar al array la lista. O directamente en mostrar hacer la funcion async-await pero tendria problemas con el spinner.
let listaMascota = await getMascotasAjaxAsync(url);
let cabecera = [];
mapCabecera();
mostrarTabla(listaMascota);

/* Boton Guardar, Modificar */
containerBotones.addEventListener("click", (e) => {
    const boton = e.target.textContent.trim();
    try {
        switch (boton) {
            case "Guardar":
                altaLista();
                break;
            case "Modificar":
                modificarLista();
                break;
            case "Cancelar":
                cancelarEdicionAnimal();
                break;
            case "Eliminar":
                eliminarAnimal();
                break;
        }
    } catch (error) {
        alert(error + error.lineNumber);
    }
});

/* Actualizacion de la tabla, creacion de la misma */
function mostrarTabla(lista) {
    if (lista.length > 0) {
        const table = crearTabla(cabecera, lista);
        btnPrincipal.setAttribute("disabled", true);

        if (containerTabla.children.length > 0) {
            containerTabla.removeChild(containerTabla.children[0]);
        }

        containerTabla.appendChild(table);
        btnPrincipal.removeAttribute("disabled");
        mapPromedio();
    }
}
function borrarTabla() {
    if (containerTabla.children.length > 0) {
        containerTabla.removeChild(containerTabla.children[0]);
    }
}

/* Alta Animal en la lista y actualizacion tabla */
async function altaLista() {
    const controles = frm.elements;
    if (validarTodosLosCampos(controles)) {
        let nuevoAnimal = new Anuncio_Animal(
            Date.now(),
            frm.titulo.value,
            frm.descripcion.value,
            frm.animal.value,
            frm.precio.value,
            frm.raza.value,
            frm.nacimiento.value,
            frm.vacuna.value
        );
        listaMascota.push(nuevoAnimal);

        borrarTabla(listaMascota);
        await postMascotaAjaxAsync(url, nuevoAnimal);
        mostrarTabla(listaMascota);

        limpiarCamposFrm();
    }
}

/* Modificar Animal en la lista y actualizacion tabla */
function modificarLista() {
    let animalEditado = new Anuncio_Animal(
        listaMascota[id].id,
        frm.titulo.value,
        frm.descripcion.value,
        frm.animal.value,
        frm.precio.value,
        frm.raza.value,
        frm.nacimiento.value,
        frm.vacuna.value
    );

    listaMascota[id] = animalEditado;

    borrarTabla();
    limpiarCamposFrm();
    putMascotaAjaxPromise(url, animalEditado)
        .catch((error) => {
            throw error;
        })
        .finally(() => {
            mostrarTabla(listaMascota);
            id = null;
            btnPrincipal.childNodes[2].textContent = "Guardar";
            funcionesScript.eliminarBotonCancelar();
            funcionesScript.eliminarBotonEliminar();
        });
}

/* Cancelar edicion Animal*/
function cancelarEdicionAnimal() {
    funcionesScript.eliminarBotonCancelar();
    funcionesScript.eliminarBotonEliminar();
    id = null;
    btnPrincipal.childNodes[2].textContent = "Guardar";
    limpiarCamposFrm();
}

/* Eliminar Animal*/
function eliminarAnimal() {
    if (id) {
        let idMascota = listaMascota[id].id;
        listaMascota.splice(id, 1);

        borrarTabla();
        limpiarCamposFrm();
        deleteMascotaAjaxPromise(url, idMascota)
            .catch((error) => {
                throw error;
            })
            .finally(() => {
                id = null;
                btnPrincipal.childNodes[1].textContent = "Guardar";
                funcionesScript.eliminarBotonCancelar();
                funcionesScript.eliminarBotonEliminar();
                mostrarTabla(listaMascota);
            });
    }
}

/* Limpiar lista */
const limpiarCamposFrm = () => {
    frm.reset();
};

/* Burbujeo del DOM containerTabla para setear los campos y modificar */
containerTabla.addEventListener("click", (e) => {
    if (e.target.matches("tr td")) {
        id = e.target.parentElement.getAttribute("data-id");

        btnPrincipal.childNodes[2].textContent = "Modificar";

        frm.titulo.value = e.target.parentElement.children[0].textContent;
        frm.descripcion.value = e.target.parentElement.children[1].textContent;
        frm.animal.value = e.target.parentElement.children[2].textContent;
        frm.precio.value = e.target.parentElement.children[3].textContent;
        frm.raza.value = e.target.parentElement.children[4].textContent;
        frm.nacimiento.value = e.target.parentElement.children[5].textContent;
        frm.vacuna.value = e.target.parentElement.children[6].textContent;

        if (containerBotones.children.length === 1) {
            funcionesScript.crearBotonEliminar();
            funcionesScript.crearBotonCancelar();
        }
    }
});

window.addEventListener("load", (e) => {
    limpiarCamposFrm();
});

containerFiltros.addEventListener("click", (e) => {
    if (e.target.matches("input[type=checkbox]")) {
        borrarTabla();
        mapCabecera();
        mostrarTabla(listaMascota);
    }
});

document.getElementById("select-filtro").addEventListener("click", (e) => {
    borrarTabla();
    mostrarTabla(listaMascota);
});

function mapCabecera() {
    let filtro = document.getElementById("select-filtro").value;
    listaMascota.map((e) => {
        if (filtro == "todo") {
            return e;
        } else if (filtro == "gato" && e.animal == "Gato") {
            return e;
        } else if (filtro == "gato" && e.animal == "Perro") {
            return e;
        }
    });
    const elementos = [
        ...document.getElementById("form-filtro").elements,
    ].filter((element) => element.type == "checkbox");

    cabecera = [
        ...elementos.map((e) => {
            if (e.checked) return e.name;
        }),
    ];
}

function mapPromedio() {
    let promedio = document.getElementById("input-promedio");
    promedio.value = listaMascota.reduce((prev, current, index) => {
        return (prev.precio + current.precio) / (index + 1);
    });
}
