document.addEventListener("click", (e) => {
    const emisor = e.target;
    if (emisor.matches("#divBotones button")) {
        //console.log(emisor);
        handlerBotonNumeros(e);
    } else if (emisor.matches("button")) {
        handlerBotonSaludar();
        //console.log("Soy el boton saludar");
    }
});

function handlerBotonNumeros() {
    console.log("Soy un boton numero");
}

function handlerBotonSaludar() {
    console.log("Soy el boton saludar");
}
