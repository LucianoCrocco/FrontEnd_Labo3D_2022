export default class Anuncio {
    constructor(id, titulo, descripcion, animal, precio) {
        try {
            this.id = id;
            this.titulo = titulo;
            this.descripcion = descripcion;
            this.animal = animal;
            this.precio = precio;
        } catch (error) {
            throw error;
        }
    }
}
