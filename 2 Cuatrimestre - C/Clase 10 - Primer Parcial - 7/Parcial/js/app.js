import {
    validarCampoVacio,
    validarPrecio,
    validarTexto,
    validarTodosLosCampos,
    clearError,
} from "./validaciones.js";
import { funcionesScript } from "./scripts.js";
import Anuncio_Animal from "./Anuncio_Animal.js";
import crearTabla from "./tabla.js";

const controles = document.forms[0].elements;
const frm = document.getElementById("form-principal");

for (let i = 0; i < controles.length; i++) {
    const control = controles.item(i);
    if (control.matches("input")) {
        if (control.matches("[type=text]")) {
            control.addEventListener("blur", validarCampoVacio);
            control.addEventListener("input", validarTexto);
        } else if (control.matches("[type=number]")) {
            control.addEventListener("blur", validarCampoVacio);
            control.addEventListener("input", validarPrecio);
        } else if (control.matches("[type=date]")) {
            control.addEventListener("input", validarTexto);
            control.addEventListener("blur", validarCampoVacio);
        }
    }
}

// FORM ABM
/* Elementos del DOM */
const btnPrincipal = document.getElementById("btnPrincipal");
const containerTabla = document.getElementById("tabla-container");
const containerBotones = document.getElementById("botones-container");

/* ID general para modificar o eliminar */
let id = null;

/* Lista del LocalStorage y generacion de la tabla */
const listaMascota = localStorage.getItem("mascotas")
    ? JSON.parse(localStorage.getItem("mascotas"))
    : [];
actualizarTabla(listaMascota);

/* Boton Guardar, Modificar */
containerBotones.addEventListener("click", (e) => {
    const boton = e.target.textContent.trim();
    try {
        switch (boton) {
            case "Guardar":
                altaLista();
                break;
            case "Modificar":
                modificarLista();
                break;
            case "Cancelar":
                cancelarEdicionAnimal();
                break;
            case "Eliminar":
                eliminarAnimal();
                break;
        }
    } catch (error) {
        alert(error);
    }
});

/* Actualizacion de la tabla, creacion de la misma */
function actualizarTabla(lista) {
    if (lista.length > 0) {
        const table = crearTabla(lista);
        btnPrincipal.setAttribute("disabled", true);

        if (containerTabla.children.length > 0) {
            containerTabla.removeChild(containerTabla.children[0]);
        }

        funcionesScript.cargarSpinner();

        setTimeout(() => {
            containerTabla.appendChild(table);
            btnPrincipal.removeAttribute("disabled");
            funcionesScript.eliminarSpinner();
        }, 500);
    } else {
        if (containerTabla.children.length > 0) {
            containerTabla.removeChild(containerTabla.children[0]);
            funcionesScript.cargarSpinner();
            setTimeout(() => {
                funcionesScript.eliminarSpinner();
            }, 500);
        }
    }
}

/* Alta Animal en la lista y actualizacion tabla */
function altaLista() {
    const controles = frm.elements;
    if (validarTodosLosCampos(controles)) {
        let nuevoAnimal = new Anuncio_Animal(
            Date.now(),
            frm.titulo.value,
            frm.descripcion.value,
            frm.animal.value,
            frm.precio.value,
            frm.raza.value,
            frm.nacimiento.value,
            frm.vacuna.value
        );
        listaMascota.push(nuevoAnimal);

        actualizarTabla(listaMascota);
        localStorage.setItem("mascotas", JSON.stringify(listaMascota));

        limpiarCamposFrm();
    }
}

/* Modificar Animal en la lista y actualizacion tabla */
function modificarLista() {
    let animalEditado = new Anuncio_Animal(
        Date.now(),
        frm.titulo.value,
        frm.descripcion.value,
        frm.animal.value,
        frm.precio.value,
        frm.raza.value,
        frm.nacimiento.value,
        frm.vacuna.value
    );

    listaMascota[id] = animalEditado;

    actualizarTabla(listaMascota);
    localStorage.setItem("mascotas", JSON.stringify(listaMascota));

    id = null;
    limpiarCamposFrm();
    btnPrincipal.childNodes[2].textContent = "Guardar";
}

/* Cancelar edicion Animal*/
function cancelarEdicionAnimal() {
    funcionesScript.eliminarBotonCancelar();
    funcionesScript.eliminarBotonEliminar();
    id = null;
    btnPrincipal.childNodes[2].textContent = "Guardar";
    limpiarCamposFrm();
}

/* Eliminar Animal*/
function eliminarAnimal() {
    if (id) {
        listaMascota.splice(id, 1);

        id = null;
        btnPrincipal.childNodes[1].textContent = "Guardar";
        localStorage.setItem("mascotas", JSON.stringify(listaMascota));
        limpiarCamposFrm();

        funcionesScript.eliminarBotonCancelar();
        funcionesScript.eliminarBotonEliminar();
        actualizarTabla(listaMascota);
    }
}

/* Limpiar lista */
const limpiarCamposFrm = () => {
    frm.titulo.value = "";
    clearError(frm.titulo);
    frm.animal[0].checked = true;
    frm.animal[1].checked = false;
    frm.descripcion.value = "";
    clearError(frm.descripcion);
    frm.precio.value = "";
    clearError(frm.precio);
    frm.raza.value = "";
    clearError(frm.raza);
    frm.nacimiento.value = "";
    clearError(frm.nacimiento);
    frm.vacuna.children[0].selected = true;
};

/* Burbujeo del DOM containerTabla para setear los campos y modificar */
containerTabla.addEventListener("click", (e) => {
    if (e.target.matches("tr td")) {
        id = e.target.parentElement.getAttribute("data-id");

        btnPrincipal.childNodes[2].textContent = "Modificar";

        frm.titulo.value = e.target.parentElement.children[0].textContent;
        frm.descripcion.value = e.target.parentElement.children[1].textContent;
        frm.animal.value = e.target.parentElement.children[2].textContent;
        frm.precio.value = e.target.parentElement.children[3].textContent;
        frm.raza.value = e.target.parentElement.children[4].textContent;
        frm.nacimiento.value = e.target.parentElement.children[5].textContent;
        frm.vacuna.value = e.target.parentElement.children[6].textContent;

        if (containerBotones.children.length === 1) {
            funcionesScript.crearBotonEliminar();
            funcionesScript.crearBotonCancelar();
        }
    }
});

window.addEventListener("load", (e) => {
    limpiarCamposFrm();
});
