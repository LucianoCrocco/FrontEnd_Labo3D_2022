export default function crearCarta(elemento) {
    const article = document.createElement("article");
    const titulo = document.createElement("h3");
    const descripcion = document.createElement("h3");
    const animal = document.createElement("h3");
    const precio = document.createElement("h3");
    const raza = document.createElement("h3");
    const fechaNacimiento = document.createElement("h3");
    const vacuna = document.createElement("h3");
    const button = document.createElement("Button");

    article.appendChild(titulo);
    article.appendChild(document.createElement("p"));
    article.appendChild(descripcion);
    article.appendChild(document.createElement("p"));
    article.appendChild(animal);
    article.appendChild(document.createElement("p"));
    article.appendChild(precio);
    article.appendChild(document.createElement("p"));
    article.appendChild(raza);
    article.appendChild(document.createElement("p"));
    article.appendChild(fechaNacimiento);
    article.appendChild(document.createElement("p"));
    article.appendChild(vacuna);
    article.appendChild(document.createElement("p"));
    article.appendChild(button);

    let i = 1;
    Object.keys(elemento).forEach((key) => {
        if (key !== "id") {
            console.log(key);
            article.children[i].textContent = elemento[key];
            i += 2;
        }
    });

    article.classList.add("card");

    titulo.textContent = "Titulo";
    descripcion.textContent = "Descripcion";
    animal.textContent = "Animal";
    precio.textContent = "Precio";
    raza.textContent = "Raza";
    fechaNacimiento.textContent = "Fecha de Nacimiento";
    vacuna.textContent = "¿Vacunado?";
    button.textContent = "Mirar Articulo";

    return article;
}
