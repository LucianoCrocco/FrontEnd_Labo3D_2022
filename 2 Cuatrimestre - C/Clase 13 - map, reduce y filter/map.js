//Recorre un array y devuelve uno nuevo modificado segun lo que se le pase en el callback
//https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Array/map
const numeros = [1, 2, 3, 4, 5];
const numerosDobles = numeros.map((e) => e * 2);
console.log(numerosDobles);

const nombres = ["Juan", "Pedro", "Carlos"];
const nombresCompuestos = nombres.map((e, index, array) => {
    console.log(`Nombre actual ${e}`);
    console.log(`Index ${index}`);
    console.log(`Array ${array}`);
    return e + " Compuesto";
});
console.log(nombresCompuestos);
