import {
    validarCampoVacio,
    validarEmail,
    validarExtension,
    validarPassword,
} from "./validaciones.js";

console.log(document.forms[0].email);
const controles = document.forms[0].elements;
console.log(controles);
/*for (let i = 0; i < controles.length; i++) {
    if (controles.item(i).matches("[type=email]"))
        console.log(controles.item(i));
}*/

const formulario = document.forms[0];
let flag = 0;
const { txtNombre, txtEmail, txtPassword, txtPassword2, ctrlFile } = formulario;

/*formulario.addEventListener("submit", (e) => {
    e.preventDefault();
    validarCampoVacio(txtNombre);
});*/
//txtNombre.addEventListener("blur", validarCampoVacio);
//txtNombre.addEventListener("keydown", validarCampoVacio);
//txtNombre.addEventListener("blur", validarCampoVacio);

for (let i = 0; i < controles.length; i++) {
    const control = controles.item(i);
    if (control.matches("input")) {
        if (
            control.matches("[type=email]") ||
            control.matches("[type=text]") ||
            control.matches("[type=password]")
        ) {
            if (control.matches("[type=text]"))
                control.addEventListener("blur", validarCampoVacio);
            if (control.matches("[type=email]"))
                control.addEventListener("input", validarEmail);
            else if (control.matches("[type=password]"))
                control.addEventListener("input", validarPassword);
        } else if (control.matches("[type=file]")) {
            control.addEventListener("change", validarExtension);
        }
    }
}

formulario.addEventListener("submit", (e) => {
    e.preventDefault();
    const controles = e.target.elements;
    console.log(controles);
    for (const control of controles) {
        if (control.classList.contains("inputError") || !control.value)
            if (
                control.matches("[type=email]") ||
                control.matches("[type=text]") ||
                control.matches("[type=password]") ||
                control.matches("[type=file]")
            ) {
                alert("error");
                break;
            }
    }
});
