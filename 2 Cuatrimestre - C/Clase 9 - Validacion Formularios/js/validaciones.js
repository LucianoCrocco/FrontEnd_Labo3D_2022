export const validarCampoVacio = (e) => {
    const input = e.target;
    !input.value.trim()
        ? setError(input, "Campo requerido")
        : clearError(input);
};

export const validarEmail = (e) => {
    const pattern = /[A-Z0-9._%+-]+@[A-Z0-9-]+.+[A-Z]{2,4}/gim;
    const input = e.target;
    const email = input.value.trim();
    if (validarLongitudMinima(input, 6))
        pattern.test(email)
            ? clearError(input)
            : setError(input, "Email invalido");
    else setError(input, "Debe tener al menos 6 caracteres");
};

export const validarPassword = (e) => {
    const pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/gm;
    const input = e.target;
    const password = input.value.trim();
    if (validarLongitudMinima(input, 8))
        pattern.test(password)
            ? clearError(input)
            : setError(
                  input,
                  "Debe contener al menos una minuscula, una mayuscula y un caracter especial"
              );
    else setError(input, "Debe tener al menos 8 caracteres");
};

export const validarExtension = (e) => {
    const extensionesValidas = ["gif", "jpg", "png", "jpeg", "webp"];
    const input = e.target;
    const nombre = input.files[0].name;
    const extension = nombre.split(".").pop();

    extensionesValidas.includes(extension)
        ? clearError(input)
        : setError(input, "Archivo invalido");
};

const validarLongitudMinima = (input, minimo) =>
    input.value.trim().length >= minimo;

const setError = (input, mensaje) => {
    const $small = input.nextElementSibling;
    const name = input.name;
    $small.textContent = mensaje || `${name.toUpperCase()} campo Requerido`;
    $small.classList.add("danger");
    input.classList.add("inputError");
};

const clearError = (input, mensaje) => {
    const $small = input.nextElementSibling;
    $small.textContent = mensaje || "";
    input.classList.remove("inputError");
    $small.classList.remove("danger");
};
